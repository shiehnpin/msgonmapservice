package server

import (
	"encoding/json"
	"fmt"
	"github.com/ziutek/mymysql/mysql"
	_ "github.com/ziutek/mymysql/native" // Native engine
	"log"
	"net/http"
	"strconv"
	"time"
)

const (
	period     = 15
	timeLayout = "2006-01-02 15:04:05.999999999"
)

var db DataSource

type RawData struct {
	Timestamp string
	UUID      string
	Lat       float64
	Long      float64
	Type      int
	Size      int
}

type DataSource interface {
	addData(time.Time, RawData)
	getAllData(time.Time, time.Time) map[time.Time]RawData
	getUserData(time.Time, time.Time, string) map[time.Time]RawData
}

type MemSource map[time.Time]RawData
type SQLSource struct{ mysql.Conn }

func AddUserData(w http.ResponseWriter, request *http.Request) {
	request.ParseForm()
	jsonData := make([]byte, request.ContentLength)
	t := time.Now()

	if _, err := request.Body.Read(jsonData); err != nil {
		log.Println("Unable to read the JSON request ", err)
	}

	var raw RawData

	if err1 := json.Unmarshal(jsonData, &raw); err1 != nil {
		log.Println("Unable to unmarshall the JSON request", err1)
	}

	raw.Timestamp = t.Format(timeLayout)
	db.addData(t, raw)

}

func getJsonString(t time.Time, IsAlluser bool, uuid string) string {

	currentTime := t
	prevoiustime := t.Add(-period * time.Minute)
	log.Println("Fetch(", uuid, "):", currentTime.Format(time.Kitchen), "->", prevoiustime.Format(time.Kitchen))

	s := ""

	if IsAlluser {
		for _, v := range db.getAllData(prevoiustime, currentTime) {
			result, _ := json.Marshal(v)
			s += string(result) + "\n"
		}
		return s
	} else {
		for _, v := range db.getUserData(prevoiustime, currentTime, uuid) {
			result, _ := json.Marshal(v)
			s += string(result) + "\n"
		}
		return s
	}

}

// func SingleUserHandleFunc(w http.ResponseWriter, requset *http.Request) {
// 	uuid := requset.URL.Path[len("/get/"):]
// 	w.Header().Set("Access-Control-Allow-Origin", "null")
// 	fmt.Fprint(w, getJsonString(time.Now(), false, uuid))
// }

func UserHandleFunc(w http.ResponseWriter, requset *http.Request) {
	requset.ParseForm()
	w.Header().Set("Access-Control-Allow-Origin", "null")

	if uuid := requset.Form.Get("UUID"); uuid != "" {
		fmt.Fprint(w, getJsonString(time.Now(), false, uuid))
	} else {
		fmt.Fprint(w, getJsonString(time.Now(), true, ""))
	}

}

func (m *MemSource) addData(t time.Time, r RawData) {
	src := map[time.Time]RawData(*m)
	src[t] = r
}

func (m *MemSource) getAllData(prev time.Time, cur time.Time) map[time.Time]RawData {
	src := make(map[time.Time]RawData)
	for timeStamp, v := range *m {
		if timeStamp.Before(cur) && timeStamp.After(prev) {
			src[timeStamp] = v
		}
	}
	return src
}

func (m *MemSource) getUserData(prev time.Time, cur time.Time, uuid string) map[time.Time]RawData {
	src := make(map[time.Time]RawData)
	for timeStamp, v := range *m {
		if v.UUID == uuid && timeStamp.Before(cur) && timeStamp.After(prev) {
			src[timeStamp] = v
		}
	}
	return src
}

func (s *SQLSource) addData(t time.Time, r RawData) {
	stmt, _ := s.Prepare("insert into usercheckdata values (?,?,?,?,?,?)")

	if _, err := stmt.Run(t.Format(timeLayout), r.UUID, r.Lat, r.Long, r.Size, r.Type); err != nil {
		log.Println(err)
	}
}
func (s *SQLSource) getAllData(prev time.Time, cur time.Time) map[time.Time]RawData {
	src := make(map[time.Time]RawData)

	rows, _, err := s.Query("select * from usercheckdata where TimeStamp > '%s' AND TimeStamp < '%s'", prev.Format(timeLayout), cur.Format(timeLayout))
	if err != nil {
		log.Println(err)
	}

	for _, row := range rows {

		tmp := RawData{}
		t := row.Time(0, time.Local)
		tmp.Timestamp = t.Format(timeLayout)
		tmp.UUID = row.Str(1)
		tmp.Lat = row.Float(2)
		tmp.Long = row.Float(3)
		tmp.Size = row.Int(4)
		tmp.Type = row.Int(5)
		src[t] = tmp
	}

	return src
}
func (s *SQLSource) getUserData(prev time.Time, cur time.Time, uuid string) map[time.Time]RawData {
	src := make(map[time.Time]RawData)

	rows, _, err := s.Query("select * from usercheckdata where UUID ='%s' && TimeStamp > '%s' AND TimeStamp < '%s'", uuid, prev.Format(timeLayout), cur.Format(timeLayout))
	if err != nil {
		log.Println(err)
	}

	for _, row := range rows {

		tmp := RawData{}
		t := row.Time(0, time.Local)
		tmp.Timestamp = t.Format(timeLayout)
		tmp.UUID = row.Str(1)
		tmp.Lat = row.Float(2)
		tmp.Long = row.Float(3)
		tmp.Size = row.Int(4)
		tmp.Type = row.Int(5)
		src[t] = tmp
	}

	return src
}

func InitDataSource(address string, user string, passwd string, dbname string) {
	sql := mysql.New("tcp", "", address, user, passwd, dbname)
	if err := sql.Connect(); err != nil {
		log.Println(err, "Using Mem Mode")
		db = &MemSource{}
	} else {
		db = &SQLSource{sql}
	}
}

func RunService(port int) {

	http.HandleFunc("/post", AddUserData)
	http.HandleFunc("/get", UserHandleFunc)
	//http.HandleFunc("/get/", SingleUserHandleFunc)

	if err := http.ListenAndServe(":"+strconv.Itoa(port), nil); err != nil {
		log.Fatal("ListenAndServe: ", err)
	}

	// stmt, err := sqldb.Prepare("INSERT INTO usercheckdata VALUES ('2014-01-14 11:30:45', 'User1', 0.1, 0.1, 1100, 1)")
	// log.Println(stmt, ",", err)

	// if _, err := stmt.Run(); err != nil {
	// 	log.Println("Error Insert")
	// }

}
