package main

import (
	"github.com/MsgOnMapService/poster"
	"github.com/MsgOnMapService/server"
	"time"
)

func main() {

	//server.InitDataSource("127.0.0.1:3306", "msguser", "1234", "msgonmapservice")
	server.InitDataSource("", "", "", "")
	go server.RunService(8080)
	for i := 0; i < 10; i++ {
		go poster.NewRandUser("http://localhost:8080/post", time.Minute)
		time.Sleep(time.Second * 5)
	}

	select {}
}
