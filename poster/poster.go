package poster

import (
	"encoding/json"
	//"fmt"
	"github.com/MsgOnMapService/server"
	"log"
	"math/rand"
	"net/http"
	"strconv"
	"strings"
	"time"
)

func generateUser() server.RawData {

	r := rand.New(rand.NewSource(time.Now().UnixNano()))

	randData := server.RawData{}
	randData.UUID = "User" + strconv.Itoa(r.Intn(100))
	randData.Size = (r.Intn(9999)) + 1
	randData.Type = (r.Intn(4)) + 1

	//Start Point
	randData.Lat = float64(r.Intn(160) - 80)
	randData.Long = float64(r.Intn(360) - 180)

	return randData
}
func generateUserData(r server.RawData) server.RawData {

	s := rand.New(rand.NewSource(time.Now().UnixNano()))
	r.Lat += s.Float64()
	r.Long += s.Float64()

	return r
}

func SendDataPostMethod(data server.RawData, _address string) {

	client := &http.Client{}
	encodingData, _ := json.Marshal(data)
	body := strings.NewReader(string(encodingData))
	request, err := http.NewRequest("POST", _address, body)

	if err != nil {
		log.Println("Fatal error ", err.Error())
	}

	request.Header.Add("Content-Type:", "application/json")
	resp, err := client.Do(request)

	if err != nil {
		log.Println("Fatal error ", err.Error())
	}

	if resp.StatusCode == 200 {
	}

	log.Println(data.UUID, "@(", data.Lat, ",", data.Long, ")")

}

func NewRandUser(_address string, d time.Duration) {

	r := generateUser()
	c := time.Tick(d)
	SendDataPostMethod(generateUserData(r), _address)
	for {
		select {
		case <-c:
			SendDataPostMethod(generateUserData(r), _address)
		}
	}

}
